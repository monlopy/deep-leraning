import tensorflow as tf
from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt
import streamlit as st
import os
from PIL import Image

st.title("Fashion mnist")
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
fashion_mnist = keras.datasets.fashion_mnist
(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()
class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
               'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']
train_images = train_images / 255.0
test_images = test_images / 255.0
model = keras.Sequential([
        keras.layers.Flatten(input_shape=(28, 28)),
        keras.layers.Dense(128, activation='relu'),
        keras.layers.Dense(10, activation='softmax')])
model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])
test_loss, test_acc = model.evaluate(test_images,  test_labels, verbose=2)
history = model.fit(train_images, train_labels, epochs=10,validation_data=(test_images,test_labels))
print(history.history.keys())

print('\nTest accuracy:', test_acc)
predictions = model.predict(test_images)
model.save('my_model.h5')


@st.cache()
def plot_image(i, predictions_array, true_label, img):

  predictions_array, true_label, img = predictions_array, true_label[i], img[i]
  plt.grid(False)
  plt.xticks([])
  plt.yticks([])

  plt.imshow(img, cmap=plt.cm.binary)

  predicted_label = np.argmax(predictions_array)
  if predicted_label == true_label:
    color = 'blue'
  else:
    color = 'red'

  plt.xlabel("{} {:2.0f}% ({})".format(class_names[predicted_label],
                                100*np.max(predictions_array),
                                class_names[true_label]),
                                color=color)

if __name__=="__main__":
    select = st.selectbox("Виберіть один із варіантів",["Loss graphic","Result after training"])
    if select=="Loss graphic":
        st.subheader("Result")
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        plt.title('Model loss')
        plt.ylabel('Loss')
        plt.xlabel('Epoch')
        plt.legend(['Train', 'Test'], loc='upper left')
        st.pyplot()
    if select=="Result after training":
        num_rows = st.slider('Вберіть кількість рядків', 1, 10)
        num_cols = st.slider('Вберіть кількість стовпців', 1, 10)
        num_images = num_rows * num_cols
        plt.figure(figsize=(2 * 2 * num_cols, 2 * num_rows))
        for i in range(num_images):
            plt.subplot(num_rows, 2 * num_cols, 2 * i + 1)
            res = plot_image(i, predictions[i], test_labels, test_images)
        plt.tight_layout()
        plt.savefig('image.png')
        st.subheader("Result")
        img=Image.open('image.png')
        st.image(img,width=800)
